---
title: "Project09"
author: "Joshua Tolhuis"
date: "9/11/2020"
output: html_document
---

```{r echo=FALSE}
# Loading in the libraries.
library("haven")
library("ggplot2")
library("tidyr")
```

```{r}
#Loading in the data with the use of haven.
data <- read_sav("Data/Datafile_Eosinofiele_Oesofagitis.sav")

#I cut of all data which wasn't relevant. More info about the cut off is in the appendix
data_cut_off <- data[- c(41:79) ,- c(27:41, 108:630)]
rowSums(data_cut_off, is.Coord(NA))
#I put everything together it's respective class
Suppl <- data_cut_off[12:18]
Suppl6wk <- data_cut_off[19:26]
SDI <-data_cut_off[27:28]
PeakEOS <- data_cut_off[29:30]
histo_response <- data_cut_off[31]
nutrients <- data_cut_off[32:76]
```

#Age and Gender
A diet is very hard to perfect. Not only is it hard to find the optimum for 1 person. But it's also not the same for every individual, therefore first a plot to show the range of this project's "test-subjects".
```{r}
# Creating a factor of the Genders
data_cut_off$Gender <- factor(data_cut_off$Gender, labels = c("Women","Men"))

#Plotting Genders Vs Age
ggplot(data = data_cut_off, mapping = aes(y = Age, x = Gender, color = Gender)) + 
  geom_jitter(width = 0.1, height = 0)
```

#WIP (correlation)
```{r}
cor(PeakEOS)
```